function createCard(name, description, picture_url, startDate, endDate, location) {
    const formattedStartDate = new Date(startDate).toLocaleDateString();
    const formattedEndDate = new Date(endDate).toLocaleDateString();
    return `
    <div class="card shadow p-2 mb-4">
        <img src="${picture_url}" class ="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer text-muted">
            <small>Dates: ${formattedStartDate} - ${formattedEndDate}
        </div>
    </div>
    `;
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error("Response was not ok")
        } else {
            const data = await response.json();

            let index = 0

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, startDate, endDate, location);
                    const column = document.querySelector(`#column-${index%3}`);
                    column.innerHTML += html;
                    index++;
                }
            }
        }
    } catch (e) {
        console.error("There is an error:", e);
    }
});
